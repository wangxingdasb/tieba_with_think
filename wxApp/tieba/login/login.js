// tieba/login/login.js
var utils = require("../../utils/util.js");
var http = utils.http;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    user_postId: 0,
    name: '',
    rowData: {}
  },

  checkLogin: function(e) {
    var url = '/user/login';
    console.log(e);
    var _this = this;
    if (e.detail.value.name == "" || e.detail.value.password == "") {
      wx.showToast({
        title: "用户名和姓名不能为空",
        icon: 'none'
      });
      return;
    }
    http.post(url, {
      name: e.detail.value.name,
      password: e.detail.value.password
    }, function(resp) {
      console.log("登陆成功的用户", resp);
      if (resp.data.info == "登录成功") {
        wx.showToast({
          title: resp.data.info
        })
        // 将登陆后的sessionID 存入全局变量
        getApp().globalData.userobj = resp.data.user;
        // 页面跳转
        if (_this.data.user_postId && _this.data.name) {
          console.log("跳转到发帖页面去");

          wx.redirectTo({
            url: '../post/detail/detail?id=' + _this.data.user_postId + "&name=" + _this.data.name
          })
        } else {
          setTimeout(function() {
            wx.switchTab({
              url: "../about/about",
              success: function(res) {
                var page = getCurrentPages().pop();
                if (page == undefined || page == null)
                  return;
                page.onLoad();
              }
            })
          }, 2000);

        }

      } else {
        wx.showToast({
          title: resp.data.info,
          icon: 'none'
        })
      }

    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if (options.id && options.name) {
      this.setData({
        user_postId: options.id,
        name: options.name
      });
    }

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */


  loginWithWechat: function() {
    var _this = this;
    wx.login({
      success: function(res) {
        if (res.code) {
          _this.getInfos(res.code, function(res) {
            if (res.data.info == "登录成功") {
              wx.showToast({
                title: res.data.info,
              });
              setTimeout(function(){
                wx.switchTab({
                  url: '../about/about',
                  success: function (res) {
                    var page = getCurrentPages().pop();
                    if (page == undefined || page == null)
                      return;
                    page.onLoad();
                  }
                });
              },1000);
          
            }
          });

        } else {
          wx.showToast({
            title: '登陆失败',
          })
          console.log("登陆失败", res.errMsg);
        }
      }
    });

  },

  getInfos: function(code, callback) {
    var _this = this;
    var url = "/user/weixinLogin";
    wx.getUserInfo({
      success: function(data) {
        var rawData = data.rawData;
        var signature = data.signature;
        var iv = data.iv;
        var encryptedData = data.encryptedData;

        http.get(url,{
          "code": code,
          "rawData": rawData,
          "signature": signature,
          'iv': iv,
          'enryptedData': encryptedData
        },function(resp){
          console.log("用户成功的用微信登录", resp);
          getApp().globalData.userobj = resp.data.user;
          callback(resp);
        });
        // wx.request({
        //   url: 'http://tieba_with_think.cc/user/weixinLogin',
        //   data: {
        //     "code": code,
        //     "rawData": rawData,
        //     "signature": signature,
        //     'iv': iv,
        //     'enryptedData': encryptedData
        //   },
        //   method: 'GET',
        //   success: function(resp) {
        //     console.log("用户成功的用微信登录", resp);
        //     getApp().globalData.userobj = resp.data.user;
            // wx.setStorageSync("cookie", resp.data.session3rd);
            
          // }
        // })
      }
    })
  },
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  }

});