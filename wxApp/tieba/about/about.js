// tieba/about/about.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isLogin:false,
    user:{},
    defalutAvatar:'//gss0.bdstatic.com/6LZ1dD3d1sgCo2Kml5_Y_D3/sys/portrait/item/11b5c6f5b7f2c6ebc0efb0c2375dab?t=1536224115239'
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    // 未登录
    console.log("我加载的时候。登陆了", getApp().globalData.userobj, wx.getStorageSync("cookie"));
    if (!getApp().globalData.userobj){
      this.setData({
        isLogin: false
      });
    }else{
      this.setData({
        isLogin: true,
        user: getApp().globalData.userobj 
      });
    }
    console.log("isLogin",this.data.isLogin,"当前用户为",getApp().globalData.userobj);
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {
   
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
   
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {
  
  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
  
  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
  
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {
  
  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {
  
  }
})