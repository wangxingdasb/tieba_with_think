// tieba/user/support/support.js
var utils = require("../../../utils/util.js");
var http = utils.http;
Page({

  /**
   * 页面的初始数据
   */
  data: {
      supportList:{
        list:[],
        total:1,
        page:1,
        cur:1,
      },
      info:""
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    //请求自己点赞的帖子
    var _this = this;
    _this.getAllSupport(_this.data.supportList.cur);
  },

  getAllSupport: function(p) {
    var url="/user/support";
    var _this = this;
    http.get(url,{
      p:p
    },function(resp){
      console.log("从服务器传来的点赞帖子为：",resp);
       if(resp.data.supportList.list.length>0){
         resp.data.supportList.list.cur > 1 ? resp.data.supportList.list = _this.data.supportList.list.concat(resp.data.supportList.list) : _this.data.supportList.list = resp.data.supportList.list;
       }
       _this.setData({
         supportList : resp.data.supportList
       });
    });

  },
  onReachBottom:function(){
    if(parseInt(this.data.supportList.cur) >= parseInt(this.data.supportList.page)) return;
    var currentPage = 'supportList.cur';
    this.setData({
      [currentPage]: parseInt(this.data.supportList.cur) + 1
    });
    this.getAllSupport(this.data.supportList.cur);
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },
  returnAbout: function () {
    wx.switchTab({
      url: '../../about/about',
    })
  },
  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})