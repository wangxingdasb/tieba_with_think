var utils = require("../../../utils/util.js");
var http = utils.http;
var pic = utils.picture;
Page({
  data: {
    post: {
      title: '',
      avatar: "",
      name: "",
      content: "",
      addTime: "",
    },

    userState: false,
    upLoadImgs: []
  },
  onLoad: function(options) {

  },
  postTitle: function(e) {
    var url = "/post/add";
    var _this = this;
    var time = utils.formatTime(new Date()).toString();
    time = time.slice(time.indexOf(" ") + 1, time.lastIndexOf(":"))
    // 点击发布的时候就上传图片
    if(_this.data.upLoadImgs.length!=0){
      for (var i = 0; i < _this.data.upLoadImgs.length; i++) {
        console.log("和快乐咯 ");
        wx.uploadFile({
          url: "http://tieba_with_think.cc" + url + "?sid=" + wx.getStorageSync("cookie"),
          filePath: _this.data.upLoadImgs[i],
          name: 'imgUrl',
          formData: {
            'title': e.detail.value.title,
            'addTime': time,
            'content': _this.data.contentText
          },
          header: {
            "Content-Type": "multipart/form-data",
            // 'content-type': 'application/x-www-form-urlencoded'
          },
          success: function (resp) {
            wx.switchTab({
              url: '../../index/index',
            })


          },
          fail: function (resp) {
            console.log(resp);
          }
        })
      }
    }else{
      http.post('/post/add',{
        'title': e.detail.value.title,
        'addTime': time,
        'content': _this.data.contentText
      },function(resp){
        if(resp.data.info=="发帖成功"){
          wx.showToast({
            title: resp.data.info,
          })
          wx.switchTab({
            url: '../../index/index',
          })
        }else{
          wx.showToast({
            title: resp.data.info,
          })
        }
      });
    }
   
    console.log("我在发布帖子", e.detail.value.title, e.detail.value.content);
    // var time = utils.formatTime(new Date()).toString();
    // time = time.slice(time.indexOf(" ") + 1, time.lastIndexOf(":"))
    // http.post(url, {
    //   title: e.detail.value.title,
    //   content: e.detail.value.content,
    //   imgUrl:(_this.data.upLoadImgs).toString(),
     
    // }, function(resp) {
    //   console.log(resp);
    //   if (resp.data.info == "发帖成功") {
    //     wx.redirectTo({
    //       url: '../../index/index',
    //     })
    //   }
    // });

  },

  returnIndex: function() {
    wx.switchTab({
      url: '../../index/index',
    })
  },

  // 上传图片
  chooseimg: function() {
    console.log("上传图片");
    var _this = this;
    wx.chooseImage({
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success: function(res) {
        var tempfile = res.tempFilePaths;
        console.log(tempfile);
        _this.data.upLoadImgs.push.apply(_this.data.upLoadImgs, tempfile);
        tempfile = _this.data.upLoadImgs;
        _this.setData({
          upLoadImgs: tempfile
        });

        console.log("用户选择的图片有", _this.data.upLoadImgs);
      }
    });
  },

  removeImg: function(e) {
    console.log("我要去除图片", e.currentTarget.dataset.id);
    this.setData({
      clickImgIndex: e.currentTarget.dataset.id
    });
    var tempImgs = this.data.upLoadImgs;
    tempImgs.splice(e.currentTarget.dataset.id, 1);
    this.setData({
      upLoadImgs: tempImgs
    });
    console.log(this.data.upLoadImgs);
  }
})