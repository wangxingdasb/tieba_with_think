// tieba/post/updata/updata.js
var utils = require("../../../utils/util.js");
var http = utils.http;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    post: {
      title: '',
      content: "",
      imgUrl: '',
      updataTime: "",
      updata_time: ""
    },
    id:0
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    var url = '/post/detail';
    var _this = this;
    _this.setData({
      id:parseInt(options.id)
    });
    http.get(url, {
      id:parseInt(options.id)
    },function(resp){
      console.log("服务器端传回来的编辑当前帖子信息",resp);
      _this.setData({
        post: resp.data.post
      });
    });
  },

  returnAbout:function(e){
    console.log("我被电击了");
    wx.switchTab({
      url: '../../about/about',
    })
  },
  updataTitle: function(e) {
    var url = "/post/update";
    console.log(e);
    var _this = this;
    var time = utils.formatTime(new Date()).toString();
    time = time.slice(time.indexOf(" ")+1,time.lastIndexOf(":"))
    // console.log(time);
    http.post(url, {
        id:_this.data.id,
        title:e.detail.value.title,
        content:e.detail.value.content,
        updataTime:time
    },function(resp){
      console.log("编辑帖子之后服务器传来的信息",resp);
      if(resp.data.info=="编辑成功"){
        // 跳转到我的发帖页面
        wx.redirectTo({
          url:'../../about/about'
        });
      }
      wx.showToast({
        title: resp.data.info,
      })
    }); 
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})