var utils = require("../../../utils/util.js");
var http = utils.http;
var modal = utils.modal;
Page({
  data: {
    topPosition: {
      top: 0,
      zIndex: 1,
      changePosition: 'absolute'
    },
    commenAvatar: "//gss0.bdstatic.com/6LZ1dD3d1sgCo2Kml5_Y_D3/sys/portrait/item/11b5c6f5b7f2c6ebc0efb0c2375dab?t=1536198212727",
    mask: false,
    animationData: '',
    userid: 0,
    placeholder: "",
    // 吧名
    name: "",
    collectionInfo: "收藏",
    curNum: 0,
    curArr: [],
    isshow: false,

    post: {
      title: "",
      name: "",
      avatar: "",
      content: "",
      img: [],
      addTime: ""
    },

    postCommentList: {
      list: [],
      page: 1,
      cur: 1
    }
  },

  // 初次加载获取发帖信息和回帖信息
  onLoad: function(options) {
    var _this = this;
    var detailUrl = '/post/detail';

    _this.setData({
      userid: parseInt(options.id),
      name: options.name
    });
    if (getApp().globalData.userobj != undefined) {
      console.log("目前登录的用户为", getApp().globalData.userobj);
      _this.getMyCollection(function(res){})
      var collections = wx.getStorageSync(getApp().globalData.userobj.id + '');
      console.log("获取到缓存的收藏数组", collections, _this.data.userid);
      if (collections.indexOf(_this.data.userid) != -1) {
        _this.setData({
          collectionInfo: "取消收藏"
        })
      }
    }


    http.get(detailUrl, {
      id: _this.data.userid
    }, function(resp) {

      console.log("服务器传回信息", resp, resp.data.post.img);
      _this.setData({
        post: resp.data.post
      });
    });

    _this.getCommentList(_this.data.postCommentList.cur);
    console.log("获取的用户发帖id是", _this.data.userid);
  },

  // 获取回帖列表
  getCommentList: function(p) {
    var _this = this;
    var commentUrl = '/post/commentList';
    http.get(commentUrl, {
      id: this.data.userid,
      p: p
    }, function(resp) {
      console.log("获取到的回帖列表信息;", resp, resp.data.postCommentList);
      var allCommentsNum = "postCommentList.page";
      _this.setData({
        [allCommentsNum]: resp.data.postCommentList.page
      });

      if (resp.data.postCommentList.list.length > 0) {
        parseInt(resp.data.postCommentList.cur) > 1 ? resp.data.postCommentList.list = _this.data.postCommentList.list.concat(resp.data.postCommentList.list) : _this.data.postCommentList.list = resp.data.postCommentList.list;

        _this.setData({
          postCommentList: resp.data.postCommentList
        });
      }
    });
  },

  onReachBottom: function(e) {
    var _this = this;
    if (parseInt(_this.data.postCommentList.cur) >= parseInt(_this.data.postCommentList.page)) return;

    var url = "/post/commentList";
    var curpage = "postCommentList.cur";
    _this.setData({
      [curpage]: parseInt(_this.data.postCommentList.cur) + 1
    });

    _this.getCommentList(_this.data.postCommentList.cur);
  },

  // 返回上一页

  returnIndex: function() {
    wx.switchTab({
      url: '../../index/index',
    })
  },
  // 更多操作：
  showMoreSelects: function() {
    var _this = this;
    _this.setData({
      mask: true
    });
    //  做动画
    modal.show({
      duration: 300,
      timingFunction: "ease"
    }, 0, _this);
  },
  preventHide: function() {

  },
  cancelOp: function(e) {
    this.setData({
      isshow: false
    })
  },
  _cancelModal: function(e) {
    var _this = this;
    console.log("取消对象", e);
    if (e.currentTarget.dataset.source == "mask") {
      modal.hide({
        duration: 300,
        timingFunction: "ease"
      }, -283, _this);
      setTimeout(function() {
        _this.setData({
          mask: false
        });
      }, 400);
    }

  },

  // 改变更多操作的显示
  onChangeShowState: function(e) {

    this.setData({
      isshow: !this.data.isshow,
      curNum: parseInt(e.currentTarget.dataset.num)
    });
    console.log(this.data.curNum, this.data.isshow);
  },

  onPageScroll: function(e) {
    var scrollHead = utils.scrollHead;
    var _this = this;
    scrollHead(e, _this);
  },

  // 回复帖子
  reply: function() {
    var _this = this;
    console.log("用户登陆了吗", getApp().globalData.userobj);
    if (!getApp().globalData.userobj) {
      _this.setData({
        placeholder: '马上登陆，马上回复'
      });

    } else {
      _this.setData({
        placeholder: '也来说两句'
      });
    }
    wx.redirectTo({
      url: '../commentAdd/commentAdd?id=' + _this.data.userid + "&name=" + _this.data.name + "&placeholder=" + _this.data.placeholder
    })
  },
  // 点击收藏
  clickToCollections: function() {
    var _this = this;
    var status = 0;
    if (!getApp().globalData.userobj) {
      wx.showToast({
        title: '请先登录后，再进行操作',
        icon: "none",
        duration: 2000,
        success: function() {
          setTimeout(function() {
            wx.redirectTo({
              url: '../../login/login?id=' + _this.data.userid + "&name=" + _this.data.name
            });
          }, 2000)
        }
      });

    } else {
      var collections = wx.getStorageSync(getApp().globalData.userobj.id + '') || [];
      // _this.getMyCollection(function(collections) {
       
        console.log("从服务器获取的我的收藏", collections);
        var index = collections.indexOf(_this.data.userid);
        if (index != -1) {
          status = 0;
          collections.splice(index, 1);
        } else {
          status = 1;
          collections.push(_this.data.userid);
        }
        var url = "/post/collection";
        http.post(url, {
          id: _this.data.userid,
          status: status
        }, function(resp) {
          console.log("服务器传来的收藏信息", resp);
          if (resp.data.info == "收藏成功") {
            wx.showToast({
              title: resp.data.info,
              icon: "none"
            })
            _this.setData({
              collectionInfo: "取消收藏"
            });
          } else if (resp.data.info == "取消收藏") {
            wx.showToast({
              title: '取消收藏',
              icon: "none"
            });
            _this.setData({
              collectionInfo: "收藏"
            });
          }

        });
        
      // })

    }
  },

  getMyCollection: function(callback) {
    var _this = this;
    var url = "/user/collectionList";
    http.get(url, {}, function(resp) {
      console.log("我收藏是", resp);
      var collections = [];
      for (var i in resp.data.collectionList.list) {
        collections.push(resp.data.collectionList.list[i].post.id);
      }
      wx.setStorageSync(getApp().globalData.userobj.id + '', collections);
      callback(collections);
    });

  
  },
  previewImg: function(e) {
    console.log("点击预览的图片", e);

    wx.previewImage({
      current: e.currentTarget.dataset.src,
      urls: e.currentTarget.dataset.list
    })
  },
  onShareAppMessage: function() {

  }

})