var utils = require("../../../utils/util.js");
var http = utils.http;
Page({
  data: {
    user_postId: 0,
    name: "",
    placeholder: "",
    judgements: true,
    contentText: '',
    postComment: {
      avatar: "",
      name: "",
      content: "",
      addTime: "",
      add_time: ""
    },
    upLoadImgs: [],
  },
  onLoad: function(options) {
    this.setData({
      user_postId: parseInt(options.id),
      name: options.name,
      placeholder: options.placeholder
    });
  },
  // 返回详情页
  returnDetail: function() {
    wx.redirectTo({
      // url: '../detail/detail?id='+,
    })
  },

  replyCommemt: function(e) {
    var _this = this;
    console.log("我想要获取的内容：", e, e.detail.value.imgUrl);
    _this.setData({
      contentText: e.detail.value.content,
    });
    console.log("我上传的图片为", _this.data.upLoadImgs);
    if (!_this.data.contentText && !getApp().globalData.userobj) {
      _this.setData({
        judgements: false
      });
      _this.showinfive();
    } else {
      // 提交用户的发布
      var url = "/post/commentAdd";
      console.log((_this.data.upLoadImgs).toString());
         http.post(url, {
              id: _this.data.user_postId,
              content: _this.data.contentText,
              imgUrl: (_this.data.upLoadImgs).toString()
            }, function (resp) {
              console.log("我的回帖内容时", resp);
              // 跳转回详情页
              wx.redirectTo({
                url: '../detail/detail?id=' + _this.data.user_postId + "&name=" + _this.data.name,
              })
            });
      for (var i = 0; i < _this.data.upLoadImgs.length; i++) {

        wx.uploadFile({
          url: "http://tieba_with_think.cc" + url + "?sid=" + wx.getStorageSync("cookie"),
          filePath: _this.data.upLoadImgs[i],
          name: 'imgUrl',
          formData: {
            'id': _this.data.user_postId,
            'content': _this.data.contentText
          },
          header: {
            "Content-Type": "multipart/form-data"
          },
          success: function(resp) {
            console.log("获取到的服务器上传信息为：", resp);
         
          }
        })

      }



    };


  },
  //提示框5s后隐藏
  showinfive: function() {
    var that = this;
    setTimeout(function() {
      // setTimeout 里面的this执行不为当前this对象。因此需要保存
      that.setData({
        judgements: true
      });
    }, 2000);

  },

  skipToLogin: function(e) {
    console.log(this.data.placeholder);
    var _this = this;
    if (_this.data.placeholder == "马上登陆，马上回复") {
      wx.redirectTo({
        url: '../../login/login?id=' + _this.data.user_postId + "&name=" + _this.data.name,
      })
    }
  },
  // 上传图片
  chooseimg: function() {
    console.log("上传图片");
    var _this = this;
    wx.chooseImage({
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success: function(res) {
        var tempfile = res.tempFilePaths;
        console.log(tempfile);
        _this.data.upLoadImgs.push.apply(_this.data.upLoadImgs, tempfile);
        tempfile = _this.data.upLoadImgs;
        _this.setData({
          upLoadImgs: tempfile
        });

        console.log("用户选择的图片有", _this.data.upLoadImgs);
      }
    });
  },

  removeImg: function(e) {
    console.log("我要去除图片", e.currentTarget.dataset.id);
    this.setData({
      clickImgIndex: e.currentTarget.dataset.id
    });
    var tempImgs = this.data.upLoadImgs;
    tempImgs.splice(e.currentTarget.dataset.id, 1);
    this.setData({
      upLoadImgs: tempImgs
    });
    console.log(this.data.upLoadImgs);
  }

})