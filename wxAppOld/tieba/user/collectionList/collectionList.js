// tieba/user/collectionList/collectionList.js
var utils = require("../../../utils/util.js");
var http = utils.http;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    collectionList: {
      list: [],
      total: 1,
      page: 1,
      cur: 1
    }

  },

  /**
   * 生命周期函数--监听页面加载
   */
  // 请求自己收藏的帖子
  onLoad: function(options) {
    var _this = this;
    _this.getAllCollectionList(_this.data.collectionList.cur);
  },

  getAllCollectionList: function(p) {
    var _this = this;
    var url = "/user/collectionList";
    http.get(url, {
      p: p
    }, function(resp) {
      console.log("我收藏是",resp);
      if (resp.data.collectionList.list.length > 0) {
        _this.data.collectionList.cur > 1 ? resp.data.collectionList.list = _this.data.collectionList.list.push.apply(_this.data.collectionList.list, resp.data.collectionList.list) : _this.data.collectionList.list = resp.data.collectionList.list;

        _this.setData({
          collectionList: resp.data.collectionList
        });

      }
      console.log("即将往客户端渲染我的收藏列表", _this.data.collectionList.list);
      

    });

  },

  onReachBottom: function() {
    if (parseInt(this.data.collectionList.cur) >= parseInt(this.data.collectionList.page)) return;
    var curpage = "collectionList.cur";
    this.setData({
      [curpage]: parseInt(this.data.collectionList.cur) + 1
    });
    this.getAllCollectionList(this.data.collectionList.cur);
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  },
  
  returnAbout: function () {
    wx.switchTab({
      url: '../../about/about',
    })
  }
})