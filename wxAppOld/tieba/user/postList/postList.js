// tieba/user/postList/postList.js
var utils = require("../../../utils/util.js");
var http = utils.http;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    isShow: false,
    postList: {
      list: [],
      total: 0,
      page: 1,
      cur: 1,
    }
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    this.getAllPostList(this.data.postList.cur);
  },

  getAllPostList: function(p) {
    var url = '/user/postList';
    var _this = this;
    http.get(url, {
      p: p
    }, function(resp) {
      console.log(resp);
      if (resp.data.postList.list.length > 0) {
        _this.data.postList.cur > 1 ? resp.data.postList.list = _this.data.postList.list.push.apply(_this.data.postList.list, resp.data.postList.list) : _this.data.postList.list = resp.data.postList.lsit;
        _this.setData({
          postList: resp.data.postList
        });

      }
      console.log("当前要出现我的帖子的数据为", _this.data.postList.list);

    });
  },
  onReachBottom: function() {
    if (parseInt(this.data.postList.cur >= this.data.postList.page)) return;
    var current = "postList.cur";
    this.setData({
      [current]: parseInt(this.data.postList.cur) + 1
    });
    this.getAllPostList(this.data.postList.cur);
  },
  changeShowState: function() {
    this.setData({
      isShow: !this.data.isShow
    });
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  // 删除帖子
  deleteCard: function(e) {
    console.log("准备删除自己发的帖子", e.currentTarget.dataset.id);
    var url = '/post/delete';
    wx.showModal({
      title: '删除操作',
      content: '确定要删除这个帖子吗？',
      success: function(res) {
        if (res.confirm) {
          http.post(url, {
            id: e.currentTarget.dataset.id
          }, function(resp) {
            console.log("删除帖子的服务器端传来信息", resp);
            wx.showToast({
              title: resp.data.info,
            })
          });
        }
      }
    })
  },
  // 更新帖子
  updateCard: function(e) {
    console.log("准备更新自己发的帖子", e.currentTarget.dataset.id);
    wx.redirectTo({
      url: '../../post/updata/updata?id=' + e.currentTarget.dataset.id,
    })
  },
  returnAbout:function(){
    wx.switchTab({
      url: '../../about/about',
    })
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})