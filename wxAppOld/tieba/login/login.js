// tieba/login/login.js
var utils = require("../../utils/util.js");
var http = utils.http;
Page({

  /**
   * 页面的初始数据
   */
  data: {
    user_postId: 0,
    name:''
  },

  checkLogin: function(e) {
    var url = '/user/login';
    console.log(e);
    var _this = this;
    http.post(url, {
      name: e.detail.value.name,
      password: e.detail.value.password
    }, function(resp) {
      console.log("登陆成功的用户", resp);
      if (resp.data.info == "登录成功") {
        wx.showToast({
          title: resp.data.info
        })
        // 将登陆后的sessionID 存入全局变量
        getApp().globalData.userobj = resp.data.user;
        // 页面跳转
        if (_this.data.user_postId && _this.data.name){
          console.log("跳转到发帖页面去");
          wx.redirectTo({
            url: '../post/detail/detail?id=' + _this.data.user_postId + "&name=" + _this.data.name
          })
        }
        else{
          wx.switchTab({
            url: "../about/about",
            success:function(res){
              var page = getCurrentPages().pop();
              if(page==undefined||page==null)
                return;
              page.onLoad();
              // page.onShow();
            }
          })
        }
        
      }


    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {
    if(options.id && options.name){
      this.setData({
        user_postId: options.id,
        name: options.name
      });
    }
   
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  loginWithWechat:function(){
    var _this = this;
    var timeStamp = Date.parse(new Date());
    console.log("nowTime", timeStamp);
    var myCode = wx.getStorageSync("code");
    var lastTime = wx.getStorageSync("timeClock");
    console.log(lastTime);
    // if(myCode && lastTime > timeStamp){
    //   console.log("使用上一次的code");
    //     _this.getInfos(myCode);
    // }else{
     
      wx.login({
        success: function (res) {
          if (res.code) {
            wx.setStorageSync("code",res.code);
            wx.setStorageSync("timeClock", Date.parse(new Date()) + 18000);
            _this.getInfos(res.code);
          } else {
            console.log("登陆失败", res.errMsg);
          }
        }
      });
    // }
},

  getInfos: function(code){
    wx.getUserInfo({
      success: function (data) {
        var rawData = data.rawData;
        var signature = data.signature;
        var iv = data.iv;
        var encryptedData = data.encryptedData;
        wx.request({
          url: 'http://tieba_with_think.cc/user/weixinLogin',
          data: {
            "code": code,
            "rawData": rawData,
            "signature": signature,
            'iv': iv,
            'enryptedData': encryptedData
          },
          method: 'GET',
          success: function (resp) {
            console.log(resp);
          }
        })
      }
    })
  },  
  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})