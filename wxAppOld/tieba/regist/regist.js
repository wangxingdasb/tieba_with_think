// tieba/regist/regist.js
var utils = require("../../utils/util.js");
var http = utils.http;
console.log(http);
Page({

  /**
   * 页面的初始数据
   */
  data: {
    nameCheck:1
  },
  checkName:function(e){
    if(e.detail.value.name==''){
      this.setData({
        nameCheck:0
      });
    }
  },
  // 注册
  regist: function(e) {
    var url = "/user/regist";
    console.log(e.detail);
    if (e.detail.value.name == '') {
      wx.showToast({
        title: '用户名不能为空',
        icon:'none'
      })
    } else if (e.detail.value.password == "") {
      wx.showToast({
        title: '密码不能为空',
        icon:'none'
      })
    } else if (e.detail.value.password != e.detail.value.repassword) {
      wx.showToast({
        title: '两次密码输入不一致',
        icon:'none'
      })
    } else {
      http.post(url, {
        name: e.detail.value.name,
        password: e.detail.value.password,
        repassword: e.detail.value.repassword,
        sex: e.detail.value.sex,
        slogan: e.detail.value.slogan

      }, function(resp) {
        console.log(resp);
        if (resp.data.info == "注册成功") {
          // 跳转页面
          wx.showToast({
            title: resp.data.info,
          });
          wx.switchTab({
            url: '../index/index',
          })
        }
      });
    }

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function(options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function() {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function() {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function() {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function() {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function() {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function() {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function() {

  }
})