<?php
namespace  Home\Controller;
use Think\Controller;

class UserController extends PublicController
{

    public $id;

    public function _initialize(){
        parent::_initialize();
        $this->id = (float)session('user')['id'];
    }

    public function index(){
        $this->ajaxReturn([
            'error' => 0,
            'info' => '注册成功',
            'sid' => $this->sid,
            'user' => D('User')->getInfo((int)$this->userId)
        ]);
    }

    //注册
    public function regist()
    {
        $name = $_POST['name'];
        $password = $_POST['password'];
        $repassword = $_POST['repassword'];
        $sex = $_POST['sex'];
        $slogan = $_POST['slogan'];
        $regist = D('User')->regist($name, $password, $repassword, $sex, $slogan);
        if (!$regist) {$this->ajaxReturn([
            'error' => 1,
            'info' => D('User')->getError(),
            'sid' => $this->sid,
        ]);
        }
        $this->ajaxReturn([
            'error' => 0,
            'info' => '注册成功',
            'sid' => $this->sid,
            'user' => $regist
        ]);
    }
    //登录
    public function login()
    {
        $name = $_POST['name'];
        $password = $_POST['password'];
        $user = D('User')->login($name, $password);
        $info = D('User')->getError();
        if (!$user) {
            $this->ajaxReturn([
                'error' => 1,     //错误码 0表示没有错误
                'info' => $info,  //错误说明
                'sid' => $this->sid,
            ]);
        } else {
            $this->ajaxReturn([
                'error' => 0,
                'info' => '登录成功',
                'sid' => $this->sid,
                'user' => $user
            ]);
        }
    }
    public function isLogin()
    {
        if ($this->checkLogin()) {
            echo '已登录';
            echo cookie('user_id');
        } else {
            echo '未登录';
        }
    }
    public function logout(){
        session('user', null);
        if (!$this->userId) {
            echo '已登出';
        } else {
            echo '未登出';
            echo cookie('user_id');
        }
    }
    public function collectionList()
    {
        $con = [
            'user_id' => session('user.id'),
            'status' => 1
        ];
        $data = D('Collection')->getPageList($con);
        $this->ajaxReturn([
            'collectionList' => $data,
            'sid' => $this->sid,
            'error' => 0,
            'info' => ''
        ]);
    }
    public function support()
    {
        $con = [
            'user_id' => session('user.id'),
            'status' => 1
        ];
        $data = D('Support')->getPageList($con);
        $this->ajaxReturn([
            'supportList' => $data,
            'sid' => $this->sid,
            'error' => 0,
            'info' => ''
        ]);
    }

    //我发的帖
    public function postList()
    {
        $userId = session('user.id');
        if (!$userId) {
            $this->ajaxReturn([
                'error' => 1,     //错误码 0表示没有错误
                'info' => '未登录',  //错误说明
                'sid' => $this->sid,
            ]);
        } else {
            $con = "user_id='$userId'";
            $data = D('Post')->getPageList($con);
            $this->ajaxReturn([
                    'postList' => $data,
                    'sid' => $this->sid,
                    'error' => 0,
                    'info' => ''

            ]);
        }
    }
}