<?php
namespace Home\Controller;
use Home\Model\PostModel;
use \Think\Controller as ThinkController;

class IndexController extends PublicController {
    public function index()
    {
        $postMod = new PostModel();
        $topDate = $postMod->getList(['title' => '%s%'], '',2);
        $count = $postMod->count();
        $data = $postMod->getPageList($_GET);
        $this->ajaxReturn([
            'top' => $topDate,
            'index' => [
                'postList' => $data,
                'name' => '美食吧',
                'post_num' => (int)$count,
                'slogn' => '世界美食爱好者交流中心',
                'logo' => "https://gss3.bdstatic.com/84oSdTum2Q5BphGlnYG/timg?wapp&quality=80&size=b150_150&subsize=20480&cut_x=0&cut_w=0&cut_y=0&cut_h=0&sec=1369815402&srctrace&di=2b4d40933c0e85356445a845c2084ecb&wh_rate=null&src=http%3A%2F%2Fimgsrc.baidu.com%2Fforum%2Fpic%2Fitem%2F0df3d7ca7bcb0a46447e3b486363f6246b60af67.jpg",

            ],
            'sid' => $this->sid,
            'error' => 0,
            'info' => ''

        ]);
    }
}