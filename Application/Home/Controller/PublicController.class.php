<?php
namespace Home\Controller;
use Think\Controller;

class PublicController extends Controller
{
    public $sid;
    public $user;
    public $userId;

    public function _initialize()
    {
        $this->session();
        $this->user = session('user');
        $this->userId = (float)$this->user['id'];

        $cookie_user_id = cookie('user_id');
        if ($this->userId) {
            cookie('user_id', $this->userId, 3600*24*7);
        } else {
            $cookie_user_id && cookie('user_id', null, -1);
        }

    }

    private function session()
    {
        $sid = $_REQUEST['sid'];

        $sid && session_id($sid);

        session('[start]');
        $this->sid = session_id();
    }

    public function checkLogin(){
        if($this->userId){
            return true;
        }
        return false;
    }
}