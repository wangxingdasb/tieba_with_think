<?php
namespace Home\Controller;
/**
 * 接口测试
 */

class TestController extends \Think\Controller{
	public function index(){
		if('首页'){
			$forms[] = array(
				'group'=>'首页',
				'title'  => '首页',
				'method' => 'get',
				'action' => '/',
				'target' => 'target',
				'eles' => array(
					array('name'=>'p', 'require'=>0),
					array('name'=>'perNum', 'require'=>0),
				),
			);
		}
		if('用户'){
			$forms[] = array(
				'group'=>'用户',
				'title'  => '注册',
				'method' => 'post',
				'action' => '/user/regist',
				'target' => 'target',
				'eles' => array(
					array('name'=>'name', 'require'=>0),
					array('name'=>'password', 'require'=>0),
					array('name'=>'repassword', 'require'=>0),
					array('name'=>'sex', 'type'=>'select', 'require'=>0, 'list' =>array(
						0 => '女', 1 => '男'
					)),
					array('name'=>'slogan', 'require'=>0),
				),
			);
			$forms[] = array(
				'title'  => '登录',
				'method' => 'post',
				'action' => '/user/login',
				'target' => 'target',
				'eles' => array(
					array('name'=>'name', 'require'=>0),
					array('name'=>'password', 'require'=>0),
				),
			);
			$forms[] = array(
				'title'  => '退出',
				'method' => 'post',
				'action' => '/user/logout',
				'target' => 'target',
			);
			$forms[] = array(
				'title'  => '是否登录',
				'method' => 'post',
				'action' => '/user/isLogin',
				'target' => 'target',
			);
			$forms[] = array(
				'title'  => '我发的帖',
				'method' => 'get',
				'action' => '/user/postList',
				'target' => 'target',
				'eles' => array(
					array('name'=>'p', 'require'=>0),
					array('name'=>'perNum', 'require'=>0),
				),
			);
			$forms[] = array(
				'title'  => '我的收藏',
				'method' => 'get',
				'action' => '/user/collectionList',
				'target' => 'target',
				'eles' => array(
					array('name'=>'p', 'require'=>0),
					array('name'=>'perNum', 'require'=>0),
				),
			);
			$forms[] = array(
				'title'  => '我的点赞',
				'method' => 'get',
				'action' => '/user/support',
				'target' => 'target',
				'eles' => array(
					array('name'=>'p', 'require'=>0),
					array('name'=>'perNum', 'require'=>0),
				),
			);
		}
		if('帖子'){
			$forms[] = array(
				'group'=>'帖子',
				'title'  => '发帖',
				'method' => 'post',
				'enctype'=>'multipart/form-data',
				'action' => '/post/add',
				'target' => 'target',
				'eles' => array(
					array('name'=>'title', 'require'=>0),
					array('name'=>'content', 'require'=>0),
					array('type' =>'file', 'name' =>'images[]', 'require'=>0),
					array('type' =>'file', 'name' =>'images[]', 'require'=>0),
					array('type' =>'file', 'name' =>'images[]', 'require'=>0),
				),
			);
			$forms[] = array(
				'title'  => '详情',
				'method' => 'post',
				'enctype'=>'multipart/form-data',
				'action' => '/post/detail',
				'target' => 'target',
				'eles' => array(
					array('name'=>'id', 'require'=>0)
				),
			);
			$forms[] = array(
				'title'  => '回帖',
				'method' => 'post',
				'enctype'=>'multipart/form-data',
				'action' => '/post/commentAdd',
				'target' => 'target',
				'eles' => array(
					array('name'=>'id', 'require'=>0),
					array('name'=>'content', 'require'=>0),
					array('type' =>'file', 'name' =>'images[]', 'require'=>0),
					array('type' =>'file', 'name' =>'images[]', 'require'=>0),
					array('type' =>'file', 'name' =>'images[]', 'require'=>0),
				),
			);
			$forms[] = array(
				'title'  => '删除',
				'method' => 'post',
				'enctype'=>'multipart/form-data',
				'action' => '/post/delete',
				'target' => 'target',
				'eles' => array(
					array('name'=>'id', 'require'=>0),
				),
			);
			$forms[] = array(
				'title'  => '编辑',
				'method' => 'post',
				'enctype'=>'multipart/form-data',
				'action' => '/post/update',
				'target' => 'target',
				'eles' => array(
					array('name'=>'id', 'require'=>0),
					array('name'=>'title', 'require'=>0),
					array('name'=>'content', 'require'=>0),
					array( 'type' =>'file', 'name' =>'images[]', 'require'=>0),
					array( 'type' =>'file', 'name' =>'images[]', 'require'=>0),
					array( 'type' =>'file', 'name' =>'images[]', 'require'=>0),
				),
			);
			$forms[] = array(
				'title'  => '收藏',
				'method' => 'post',
				'enctype'=>'multipart/form-data',
				'action' => '/post/collection',
				'target' => 'target',
				'eles' => array(
					array('name'=>'id', 'require'=>0),
					array('name'=>'status', 'require'=>0),
				),
			);
			$forms[] = array(
				'title'  => '点赞',
				'method' => 'post',
				'enctype'=>'multipart/form-data',
				'action' => '/post/support',
				'target' => 'target',
				'eles' => array(
					array('name'=>'id', 'require'=>0),
					array('name'=>'status', 'require'=>0),
				),
			);
		}

		$this->show2($forms);
	}
	
	function show2($forms){
		$groupArr = [];
		foreach($forms as $k=>$v){
			$v['group'] && ($group = $v['group']) && $groupArr[$v['group']] = ['group'=>$v['group'], 'list'=>[]];
			$groupArr[$group] && ($groupArr[$group]['list'][] = $v['title']);
		}
        
		$groupStr = '<div class="left-nav panel panel-default" style="width:15%;position:fixed;left:0;top:20px;bottom:0;">'
			.' <dt class="panel-heading">接口导航</dt> '.
			'<ul style="top:41px;bottom:0;margin:0; overflow-y: auto;position: absolute;width: 100%;padding-bottom:10px;padding-top:10px;">';
		foreach($groupArr as $v){
			$groupStr .= '<li class="list-item"><a href="#' . $v['group'] . '">' . $v['group'] . '</a>';
            
            $subList = '<ul>';
            foreach($v['list'] as $v2)
                $subList .= '<li class="list-item"><a href="#' . $v2 . '">' . $v2 . '</a>';
            $subList .= '</ul>';
            
            $groupStr .= $subList . '</li>';
		}
		$groupStr .= '</ul></div>';
		
		$form = new Form();
		echo '<!DOCTYPE html>
				<html lang="zh-cn">
				<head>
					<meta name="renderer" content="webkit" />
				</head><div class="document" style="position:relative;">
			'. $groupStr .'<div class="form-list pull-left" style="width:52%;margin-left:15%;">';
		foreach($forms as $k=>$v){
			echo '<div id="'. $v['group'] .'" class="panel panel-default" style="margin:20px;" >
				<div class="panel-heading" id="'. $v['title'] .'">'. ($k).'.'. $v['title'].'  '.$v['action']. '</div>
				<div class="panel-body">';
			$form->index($v);
			echo '</div>
				</div>';
		}
		echo '</div><div class="frame pull-right" style="width:32%;position:fixed;right:20px;top:20px;bottom:20px;">'.
			'<iframe width="100%" height="100%" name="target" ></iframe></div></div>';
		
		echo 
'<link rel="shortcut icon" href="Public/admin.ico" type="image/x-icon"/>
<link rel="stylesheet" type="text/css" media="screen" href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" media="screen" href="https://cdn.bootcss.com/bootstrap/3.3.7/css/bootstrap-theme.min.css">
<script type="text/javascript" src="https://cdn.bootcss.com/jquery/3.1.1/jquery.min.js"></script>
<script type="text/javascript" src="https://cdn.bootcss.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
	</body>
</html>';
	}
}


class Form{
	static $useImg = false;
	static $imgNum  = 0;
	static $fileNum  = 0;
	static $dateNodeNum = 0;
	static $daterangeNodeNum = 0;
	static $img = array(
		'label'=>'图片', 
		'name' => 'img',
		'path'	=> ''
	);
	
	function index($data){
		extract($data);
		!$method && $method = 'get';
		isset($enctype) && ($enctype = 'enctype="' . $enctype . '"');
		!$class && $class = 'form-horizontal';
		echo '<form '.$enctype.' action="'. $action. '" method="' .$method. '"  class="'. $class .'" target="'. $target .'">';
		$this->elements($eles);
		
		echo '<div class="form-group">
			 <div class="col-sm-offset-3 col-sm-9">
				<button type="submit" class="btn btn-primary">提交</button>
			 </div>
		  </div>';
		echo '</form>';
	}
	
	//生成表单元素
	function elements($data){
		foreach($data as $v){
			$this->row($v);
		}
	}
	
	/**
	 * @param [] $data ['type'=>'', name="", value=>'', 'checked'=>,'selected'=>]
	 */
	function node($data, $return = false){
		$data['return'] = $return;
		extract($data);
		$required = $require ? 'required':'';
		if('string' == $type){
			$str = $value . '<input type="hidden" name="'.$name.
				'" value="'. $value .'" '. $required .'>';
		}
		
		if('file' == $type){
			$str = '<input type="file" class="form-control" name="'. $name .
				'" value="'. $value .'" ' . $required . '>';
		}
		
		if('text' == $type || !isset($type)){
			$str = '<input type="text" placeholder="' . $placeholder . '" class="form-control" name="'. $name .
				'" value="'. $value .'" ' . $required . '>';
		}
		if('textarea' == $type){
			$str = '<textarea class="form-control" placeholder="'. $placeholder .'" rows="8" name="'. $name .
				'" ' . $required . '>'. $value .'</textarea>';
		}
		//date
		if('date' == $type){
			!isset($data['istime']) && $data['istime'] = true;
			!isset($data['format']) && ($data['format'] = 'YYYY-MM-DD hh:mm:ss');
			
			$this->assign('dateNodeNum', self::$dateNodeNum);
			$this->assign($data);
			$this->display('Widget:Form:date');
			self::$dateNodeNum++;
			return;
		}
		if('daterange' == $type){
			$str = $this->daterange($data, $return);
		}
		
		//百分比
		if('per' == $type){
			$str = '<div class="input-group"><input type="text" class="form-control" name="'. $name .
				'" value="'. $value .'" ' . $required . '><span class="input-group-addon">%</span></div>';
		}
		if('price' == $type){
			$str = '<div class="input-group"><span class="input-group-addon">￥</span><input type="text" class="form-control" name="'. $name .
				'" value="'. $value .'" ' . $required . '></div>';
		}
		//编辑器
		if('editor' == $type){
			$str = '<textarea class="form-control kind-editor" name="'. $name .
				'" ' . $required . '>'. $value .'</textarea>';
		}
		
		if('radio' == $type || 'checkbox' == $type || 'select'== $type){
			$obj = new SelectWidget();
			$str = call_user_func([$obj, $type],$data);
		}
		
		if('image' == $type){
			$str = $this->img($data);
		}
		
		if('album' == $type){
			$str = $this->album($data, $return);
		}
		
		if($return){
			return $str;
		}
		echo $str;
	}
	
	public function img($name, $label=null , $path=null,$req=false){
		$img = self::$img;
		if(is_array($name)) extract($name);

		$path  ? $img['path']  		= $path  : null;
		!$path &&  ($img['path'] = $value);
		$label ? $img['label']  	= $label : null;
		$name  ? $img['name']  		= $name  : null;
		$req   ? $img['required'] 	= true   : null;
		!$img['required'] && $img['required'] = $name['required'];
		self::$imgNum++;
		$img['idName']  .= 'img-input-id-' . self::$imgNum;
		$img['preview'] .= 'img-preview-'  . self::$imgNum;
		$this->assign('img', $img);
		$this->assign('imgL', $l);
		$this->assign('imgR', $r);
		if($return){
			return $this->fetch('Widget:Form:img');
		}
		$this->display('Widget:Form:img');
	}
	
	//相册图片
	public function album($data, $return = false){
		$list = $data['list'];
		$str = '<ul class="album" type="image">';
		foreach( $list as $k=>$v ){
			$str .= '<li rank="'. ($k+1) .'"  class="img-thumbnail '. ($v['default']?'default':'') .'">'.
				'<a href="'. $v['path'] .'" target="_blank"><img src="'. $v['path'] .'"></a>
			<input type="hidden" name="'. $data['name'] . '['. $v['id'] .']['. ($k+1) .']  value="' . $v['path'] . '">
			<span class="del"></span><span class="set-default">设为默认</span>
			</li>';
		}
		$str .= '</ul>';
		
		if($return)
			return $str;
		
		echo $str;
	}
	
	public function per(){
		
	}
	
	function row($data){
		!$data['label'] && $data['label'] = $data['name'];
		$l = $data['l'] ? $data['l'] : 3; 
		$r = $data['r'] ? $data['r'] : 9; 
		$data['l'] = $l;
		$data['r'] = $r;
		if('image' == $data['type']) {
			 echo $this->node($data, 1);
			 return;
		}
		$req = $data['require'];
		!$req && $req = $data['required'];
		$req && $req = '<i class="required-star">*</i>';
		echo '<div class="form-group">
				<div class="col-sm-'.$l.' control-label">'.$req. $data['label'] . '</div>
				<div class="col-sm-'.$r.'">
					'. $this->node($data, 1) .'
				</div>
			  </div>';
	}
}

class SelectWidget{
	/**
	 * @param array $data #键有*name,*list,selected,nameKey,valueKey,padd1,paddText
	 */
	function index($data){
		if(!$data['name'] || !$data['list'] || !is_array($data['list'])){
			return;
		}
		
		
		$valueKey = $data['valueKey'] ? $data['valueKey'] : 'id';
		$nameKey  = $data['nameKey']  ? $data['nameKey']  : 'name';
		$str = '<select';
		$data['readonly'] && $str .= ' readonly ';
		$str .= ' name="'. $data['name'] .'" class="form-control">';

		if($data['padd1'] || $data['paddText']){
			!$data['paddText'] && $data['paddText'] = '请选择';
			$str .= '<option value="">-' .$data['paddText']. '-</option>';
		}
		foreach($data['list'] as $k=>$v){
			if($data['group'] && 0 == $v['parent_id']){
				if(isset($pid)) {
					$str .= '</optgroup>';
				}
				$str.= '<optgroup label="' . $v[$nameKey] . '">';
				$pid = $v['id'];
				continue;
			}
		
			if(is_array($v)){
				$value = isset($v[$valueKey]) ? $v[$valueKey] : $k;
				$selected = ((string)$value === $data['selected'] ? 'selected':'');
				!$selected && ((string)$value === $data['value']) && ($selected = 'selected');
				
				$str .= '<option value="'. $value .'" '. $selected .'>'. 
					$v[$nameKey] .'</option>';
				continue;
			}
			
			$value = $k;
			$data['valueKey'] && $value = $v;
			$selected = ((string)$value === (string)$data['selected']) ? 'selected':'';
			!$selected  && ((string)$value === $data['value']) && ($selected = 'selected');
			
			$str .= '<option value="'. $value .'" '. $selected .'>'. 
				$v .'</option>';
		}
		$data['group'] && $str .= '</optgroup>';
		$str .= '</select> ';
		
		if($data['return'])
			return $str;
		
		echo $str;
	}
	
	function select($data){
		return $this->index($data);
	}
	
	/**
	 * 生成radio
	 **/
	function radio($data){ 
		if(!$data['name'] || !$data['list'] || !is_array($data['list'])){
			return;
		}

		$valueKey = $data['valueKey'] ? $data['valueKey'] : 'value';
		$nameKey  = $data['nameKey']  ? $data['nameKey']  : 'name';
		!$data['checked'] && $data['checked'] = $data['selected']; 
		
		$str = '';
		foreach($data['list'] as $k=>$v){
			$str .= '<label class="radio-inline">';
			if(is_array($v)){
				$value = isset($v[$valueKey]) ? $v[$valueKey] : $k;
				$str .= '<input name="'. $data['name'] .'" type="radio" value="'. $value .'" '.
					($value==$data['checked']? 'checked':null) .'>'. 
					$v[$nameKey] .'</label>';
				continue;
			}
			
			$str .= '<input name="'. $data['name'] .'" type="radio" value="'. $k .'" '. 
				($k == $data['checked']? 'checked':null) .'>'. 
				$v . '</label>';
		}
		if($data['return']) return $str;
		echo $str;
	}
	
	/**
	 * 生成checkbox
	 */
	function checkbox($data){
		$str = '';
		$list = $data;
		$data['list'] && $list = $data['list'];
		
		if($data['list'] && $data['value']){
			$arr = $data['value'];
			!is_array($arr) && $arr = explode(',', $data['value']);
		}
		
		foreach($list as $k=>$v){
			$str .= '<label class="checkbox-inline">';
			$value = isset($v[$data['valueKey']]) ? $v[$data['valueKey']] : $v['value'];
			!$value && ($value = isset($v[$v['valueKey']]) ? $v[$v['valueKey']] : $v['value']);
			!$value && ($value =  $v['name']);
			!$value && ($value =  $k);
			$nameKey = $data['name']  ? $data['name']  : 'name';
			!$nameKey && ($nameKey = $v['nameKey']  ? $v['nameKey']  : 'name');
			$checked = ($v['checked'] || $v['selected']) ? 'checked':'';
			!$checked && in_array($value, $arr) && ($checked = 'checked');
			$str .= '<input name="'. $nameKey .'['. $k .']"  type="checkbox" value="'.
				$value .'" '. $checked .'>'. $v['name'] .'</label>';
		}
		if($data['return']) return $str;
		echo $str;
	}
	
}