<?php
namespace Home\Controller;
use Home\Model\PostCommentModel;
use Home\Model\PostModel;
use \Think\Controller as ThinkController;

//帖子
class PostController extends PublicController
{
    //发布帖子
    public function add()
    {
        $title = $_POST['title'];
        $content = $_POST['content'];
        $add = D('Post')->addPost($title, $content, session('user.id'));
        $info = D('Post')->getError();
        if (!$add) {$this->ajaxReturn([
            'error' => 1,
            'info' => $info,
            'sid' => $this->sid,
        ]);
        }
        $this->ajaxReturn([
            'error' => 0,
            'info' => '发帖成功',
            'sid' => $this->sid,
            'post' => $add
        ]);
//        D('Post')->upload();
    }
    //帖子编辑
    public function update()
    {
        $id = $_POST['id'];
        $con = "id='$id'";
        $title = $_POST['title'];
        if (empty($title)) {
            $title = D('Post')->where($con)->getField('title');
        }
        $content = $_POST['content'];
        if (empty($content)) {
            $content = D('Post')->where($con)->getField('content');
        }
        $update = D('Post')->update($id, $title, $content, session('user.id'));
        $info = D('Post')->getError();
        if (!$update) {$this->ajaxReturn([
            'error' => 1,
            'info' => $info,
            'sid' => $this->sid,
        ]);
        }
        $this->ajaxReturn([
            'error' => 0,
            'info' => '编辑成功',
            'sid' => $this->sid,
            'post' => $update
        ]);
    }
    //帖子删除
    public function delete()
    {
        $id = $_POST['id'];
        $del = D('Post')->deletePost($id, session('user.id'));
        $info = D('Post')->getError();
        if ($del) {
            $this->ajaxReturn([
                'error' => 0,
                'info' => '删除成功',
                'sid' => $this->sid,
            ]);
        } else {
            $this->ajaxReturn([
                'error' => 1,
                'info' => $info,
                'sid' => $this->sid,
            ]);
        }

    }
    //回帖
    public function commentAdd()
    {
        $id = $_POST['id'];
        $content = $_POST['content'];
        $comAdd = D('PostComment')->commentAdd($id, $content, session('user.id'));
        $info = D('PostComment')->getError();
        if (!$comAdd) {$this->ajaxReturn([
            'error' => 1,
            'info' => $info,
            'sid' => $this->sid,
        ]);
        }
        $this->ajaxReturn([
            'error' => 0,
            'info' => '发帖成功',
            'sid' => $this->sid,
            'post' => $comAdd
        ]);

    }
   //帖子收藏
    public function collection()
    {
        $id = $_POST['id'];
        $status = $_POST['status'];
        $col = D('Collection')->collection($id, session('user.id'), $status);
        $info = D('Collection')->getError();
        if ($col) {
            $this->ajaxReturn([
                'error' => 0,
                'info' => $info,
                'status' => D('Collection')->getErrorCode(),
                'sid' => $this->sid,
            ]);
        } else {
            $this->ajaxReturn([
                'error' => 1,
                'info' => $info,
                'sid' => $this->sid,
            ]);
        }
    }
    //帖子点赞
    public function support()
    {
        $id = $_POST['id'];
        $status = $_POST['status'];
        $sup = D('Support')->support($id, session('user.id'), $status);
        $info = D('Support')->getError();
            if ($sup) {
            $this->ajaxReturn([
                'error' => 0,
                'info' => $info,
                'status' => D('Support')->getErrorCode(),
                'sid' => $this->sid,
            ]);
        } else {
            $this->ajaxReturn([
                'error' => 1,
                'info' => $info,
                'sid' => $this->sid,
            ]);
        }
    }
    //帖子详情
    public function detail()
    {
        //获取帖子id
        $postId = I('get.id', 1);
        //实例化帖子模型，并调用getPostDetail方法获取帖子详情数据
        $postMod = new PostModel();
        $data = $postMod->getDetail($postId);
        $this->ajaxReturn([
            'post' => $data,
            'error' => 1,
            'info' => "...",
            'sid' => $this->sid
        ]);

    }
    //回帖列表
    public function commentList()
    {
        $postId = I('get.id', 1);
        $user = D('User');
        $postComment = D('PostComment');
        $count = $postComment->where("post_li_id='$postId'")->count();
        $perNum = (int)$_GET['perNum'];
        if(!$perNum || $perNum > 50)
            $perNum = 10;

        $p = new \Think\Page($count, $perNum);
        $list = $postComment->where("post_li_id='$postId'")->limit($p->firstRow.','.$p->listRows)->select();
        //$lists = $postComment->where("id='$postId'")->select();

        foreach ($list as $k => &$v) {
            $userId = $v['user_id'];
            $v['name'] = $user->where("id='$userId'")->getField('name');
            if (date("Ym", $v['$add_time']) > 20188) {
                $v['addTime'] = date("h:i", $v['add_time']);
            } else {
                $v['addTime'] = date("Y年m月d日 h:i", $v['add_time']);
            }
            $v['avatar'] = $user->where("id='$userId'")->getField('avatar');
            if (mb_substr($v['avatar'], 0, 4) == 'uplo' && !empty($v['avatar'])) {
                $v['avatar'] = "http://tieba_with_think.cc/public/images/".substr($v['avatar'], 7);
            }
        }
//            var_dump($list);
//        $lists = $list->field('name,content,add_time,addTime,update_time,updateTime')->find();
        $this->ajaxReturn(['postCommentList' => [
            'list' => $list,
            'total' => 10, //记录总数
            'page' => 2,    //总页数
            'cur' => 1
        ],
        'error' => 1,
        'info' => "...",
        'sid' => $this->sid
        ]);
//        var_dump($lists);
//        echo 'sss';
    }

}