<?php
namespace Home\Model;
use Think\Model;
class PostCommentModel extends Model {

    public $lastError;
    protected $errorCode;
    public function index()
    {

    }

    public function commentAdd($id, $content, $userId)
    {
        if (!$userId) {
            return $this->setError('未登录');
        } else {
            $user_id = session('user.id');
            $img = $this->upload();
            if (empty($id)) {
                return $this->setError('未传入需要回复的帖子id');
            }
            if (empty($content)) {
                return $this->setError('内容不能为空!');
            }
            $data = [
                'post_li_id' => $id,
                'user_id' => $user_id,
                'content' => $content,
                'add_time' => time(),
                'img' => $img
            ];

            if ($id = $this->add($data)) {
                $info = $this->getInfo($id);
                return $info;
            }
            return false;
        }
    }

    public function getInfo($id, $field = '*')
    {
        $id = (int)$id;
        $info = $this->field($field)->find($id);
        if (!$info) return [];
        return $this->parseRow($info);
    }

    public function parseRow($v)
    {
        $v['id'] = (int)$v['id'];
        $v['img'] = explode(",", $v['img']);
        foreach ($v['img'] as $key => $value) {
            if (!empty($value) && mb_substr($v['img'][$key],0,4) != 'http') {
                $v['img'][$key] = "http://tieba_with_think.cc/public/images/".$v['img'][$key];
            }
        }

        $v['addTime'] = date("h:i", $v['add_time']);
        if ($v['update_time'] == 0) {
            $v['updateTime'] = '0';
        } else {
            $v['updateTime'] = date("h:i", $v['update_time']);
        }
        return $v;
    }

    /**
     * 上传文件
     * @return string
     */
    public function upload()
    {
        $upload = new \Think\Upload();// 实例化上传类
        $upload->maxSize   =     5145728 ;// 设置附件上传大小
        $upload->exts      =     array('jpg', 'gif', 'png', 'jpeg');// 设置附件上传类型
        $upload->rootPath  =     './Public/images/'; // 设置附件上传根目录
        $upload->savePath  =     ''; // 设置附件上传（子）目录
        $upload->saveName = '';
        $upload->autoSub = false;
        $upload->replace = true;
        // 上传文件
        $info   =   $upload->upload();
        $imgUrl = '';
//        if(!$info) {// 上传错误提示错误信息
//            echo 'sss';
//        }// 上传成功
        foreach($info as $file){
            if (empty($imgUrl)) {
                $imgUrl = $file['savepath'].$file['savename'];
            } else {
                $imgUrl .= ','.$file['savepath'].$file['savename'];
            }
        }
        return $imgUrl;

    }

    /**
     * 设置错误信息
     * @param $msg
     * @param int $code
     * @param bool $flag
     * @return bool
     */
    protected function setError($msg, $code=1,$flag = false){
        $this->lastError = $msg;
        $this->errorCode = $code;
        return $flag;
    }

    //错误码
    public function getErrorCode(){
        return $this->errorCode;
    }

    public function getError(){
        if($this->lastError)
            return $this->lastError;
        return parent::getError();
    }
}