<?php
namespace Home\Model;
use Think\Model;
class UserModel extends Model {
    public $lastError;
    protected $errorCode;
//    protected $_auto = array (
//        ///array('status','1'),  // 新增的时候把status字段设置为1
//        array('password','md5',3,'function') , // 对password字段在新增和编辑的时候使md5函数处理
//        //array('name','getName',3,'callback'), // 对name字段在新增和编辑的时候回调getName方法
//        array('update_time','time',2,'function'), // 对update_time字段在更新的时候写入当前时间戳
//        array('add_time','time',1,'function'), // 对add_time字段在新增的时候写入当前时间戳
//    );
    protected $_validate = array(
        array('name','require','用户名不能为空！！', 1),//ps: require 意思是字段必须，就是不能为空
        array('name','','帐号名称已经存在！',0,'unique',1),// 在新增的时候验证name字段是否唯一
        array('repassword','password','确认密码不正确',0,'confirm'), // 验证确认密码是否和密码一致
    );
    public function index()
    {

    }


    /**
     * 用户登录
     * @param $name
     * @param $password
     * @return mixed
     */
    public function login($name, $password)
    {
        $con['name'] = htmlentities($name);

        $user = $this->where($con)->find();

        $id = $user['id'];

        if (!($pass = $this->getPassword($password))) {
            return false;
        }

        if(!$user)
            return $this->setError('账户不存在');

        if ($pass != $user['password']) {
            return $this->setError('密码错误!');
        }

        $this->save(['login_time' => time(), 'id' => $id]);
        $user = $this->getInfo($id);

        session('user', $user);
        return $user;
    }

    /**
     * 获取加密后的密码
     * @param string $pwd
     * @return string
     */
    public function getPassword($pwd)
    {
        return md5($pwd);
    }

    /**
     * 注册
     * @param string $name
     * @param string $password
     * @param string $repassword
     * @param int $sex
     * @param string $slogan
     * @return array|bool
     */
    public function regist($name, $password, $repassword, $sex, $slogan)
    {
        $data = [
            'name' => $name,
            'password' => $this->getPassword($password),
            'repassword' => $this->getPassword($repassword),
            'sex' => $sex,
            'slogan' => $slogan
        ];

        if($id = $this->edit($data)){
            $user = $this->getInfo($id);
            //session('user', $user);
            return $user;
        }
        return false;
    }

    public function collectionList()
    {

    }

    /**
     * 用户详情
     * @param $id
     * @param string $field
     * @return array
     */
    public function getInfo($id, $field='*')
    {
        $id = (int)$id;
        $info = $this->field($field)->find($id);
        if (!$info) return [];
        return $this->parseRow($info);
    }

    /**
     * 格式化
     * @param $v
     * @return mixed
     */
    public function parseRow($v)
    {
        if (mb_substr($v['avatar'], 0, 4) == 'uplo' && !empty($v['avatar'])) {
            $v['avatar'] = "http://tieba_with_think.cc/public/images/".substr($v['avatar'], 7);
        }
        $v['id'] = (int)$v['id'];
        //$v['img'] = explode(",", $v['img']);
        $v['addTime'] = date("h:i", $v['add_time']);
        $v['loginTime'] = date("h:i", $v['login_time']);
        if ($v['update_time'] == 0) {
            $v['updateTime'] = '0';
        } else {
            $v['updateTime'] = date("h:i", $v['update_time']);
        }
        return $v;
    }

    /**
     * 编辑
     * @param $data
     * @param null $id
     * @return bool|mixed|null
     */
    public function edit($data, $id=null)
    {
        if ($id) {
            return $id;
        }

        $data['add_time'] = time();

        if (!$this->create($data)) {
            return false;
        }

        $id = $this->add($data);
        if (!$id) {
            $this->lastError = '新建用户失败';
            return false;
        }
        return $id;
    }

    /**
     * 设置错误信息
     * @param $msg
     * @param int $code
     * @param bool $flag
     * @return bool
     */
    protected function setError($msg, $code=1,$flag = false){
        $this->lastError = $msg;
        $this->errorCode = $code;
        return $flag;
    }

    //错误码
    public function getErrorCode(){
        return $this->errorCode;
    }

    public function getError(){
        if($this->lastError)
            return $this->lastError;
        return parent::getError();
    }
}