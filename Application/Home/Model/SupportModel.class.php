<?php
namespace Home\Model;
use Think\Model;
class SupportModel extends Model {
    public $lastError;
    protected $errorCode;

    public function index()
    {

    }

    public function support($id, $userId, $status)
    {
        if (!$userId) {
            return $this->setError('未登录!');
        } else {
            $user_id = session('user.id');
            $supId = $this->where("post_li_id='$id' AND user_id='$user_id'")->getField('id');
            $count = $this->where("post_li_id='$id'")->getField('count');
            if (empty($id)) {
                return $this->setError('不知道给哪个帖子点赞呢！');
            }
            if ($status == 1) {
                if ($supId == NULL && $count == NULL) {
                    $data = [
                        'user_id' => $user_id,
                        'post_li_id' => $id,
                        'add_time' => time(),
                        'status' => 1,
                        'count' => 1
                    ];
                    $this->add($data);
                    return $this->setError('点赞成功', 1, true);
                } elseif ($supId == NULL && $count != NULL) {
                    $count++;
                    $data = [
                        'user_id' => $user_id,
                        'post_li_id' => $id,
                        'add_time' => time(),
                        'status' => 1,
                        'count' => $count
                    ];
                    $this->add($data);
                    $this->where("post_li_id='$id'")->field('count')->save($data);
                    return $this->setError('点赞成功', 1, true);
                } elseif($supId != NULL) {
                    $count++;
                    $data = [
                        'status' => 1,
                        'count' => $count
                    ];
                    $this->where("post_li_id='$id'")->field('count')->save($data);
                    $this->where("post_li_id='$id' AND user_id='$user_id'")->field('status')->save($data);
                    return $this->setError('点赞成功', 1, true);
                }
            } elseif ($status == 0) {
                $count--;
                $data = [
                    'status' => 0,
                    'count' => $count
                ];
                $this->where("post_li_id='$id'")->field('count')->save($data);
                $this->where("post_li_id='$id' AND user_id='$user_id'")->field('status')->save($data);
                return $this->setError('取消点赞', 0, true);
            }
        }
        return true;
    }


    public function getPageList($con, $perNum = 15, $orderBy = 'update_time desc')
    {
        //数据分页
        $count = $this->where($con)->count();
        $perNum = (int)$_GET['perNum'];
        if(!$perNum) $perNum = 10;
        $p = new \Think\Page($count, $perNum);

        $list = $this->where($con)->limit($p->firstRow.','.$p->listRows)->select();
        $pageVar = $p -> show();
        $postIdArr = array_column($list, 'post_li_id');
        $postList = [];
        if (!empty($postIdArr)) {
            $con = ['id' => ['in', $postIdArr]];
            $postList = D('Post')->where($con)->select();
        }
        //给list添加新的字段
        foreach ($list as $k => $v) {
            $post_li_id = $v['post_li_id'];
            foreach ($postList as $key => $value) {
                if ($value['id'] == $post_li_id) {
                    $v['post'] = $value;
                }
            }
            $list[$k] = $v;
        }
        $cur = $_GET['p'];
        !$cur && $cur = 1;
        return [
            'list' => $list,
            'total' => (int)$p -> totalRows,
            'page' => (int)$p -> totalPages,
            'cur' => $cur,
            'pageVar' => $pageVar
        ];
    }

    /**
     * 设置错误信息
     * @param $msg
     * @param int $code
     * @param bool $flag
     * @return bool
     */
    protected function setError($msg, $code=1,$flag = false){
        $this->lastError = $msg;
        $this->errorCode = $code;
        return $flag;
    }

    //错误码
    public function getErrorCode(){
        return $this->errorCode;
    }

    public function getError(){
        if($this->lastError)
            return $this->lastError;
        return parent::getError();
    }
}