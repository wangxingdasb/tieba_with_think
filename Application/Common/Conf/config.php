<?php
return array(
	//'配置项'=>'配置值'
    // 添加数据库配置信息

    'SESSION_AUTO_START' =>false,
	'SESSION_OPTIONS'=>array(
		'use_trans_sid'=>1,
		'expire'=>3600,//设置过期时间session.gc_maxlifetime的值为1小时
	),

    'DB_TYPE'=>'mysql',// 数据库类型
    'DB_HOST'=>'127.0.0.1',//服务器地址
    'DB_NAME'=>'tieba', // 数据库名
    'DB_USER'=>'root',  // 用户名
    'DB_PWD'=>'1234',   // 密码
    'DB_PORT'=>3306,    // 端口
    'DB_PREFIX'=>'',    // 数据库表前缀
    'DB_CHARSET'=>'utf8',// 数据库字符集
	
	'DEFAULT_MODULE'     => 'Home', //默认模块
	'URL_MODEL'          => '2',
	'MODULE_ALLOW_LIST' => array('Home','Admin'),
);